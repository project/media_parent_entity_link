<?php

namespace Drupal\media_parent_entity_link;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class InitialSettingsService.
 *
 * Providing some initial settings for the module, to avoid hardcoding in
 * .module file and allow for easier extending to other formatters.
 */
class InitialSettingsService {

  /**
   * @var array of field formatter plugin ids
   */
  protected $formatters = ['image', 'responsive_image'];

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new InitialSettingsService object.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * @return array
   */
  public function getFormatters() {
    $formatters = $this->formatters;
    $this->moduleHandler->invokeAll('media_parent_entity_link_alter_formatters', [&$formatters]);
    return $formatters;
  }

}
