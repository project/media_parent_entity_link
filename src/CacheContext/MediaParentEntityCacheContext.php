<?php

namespace Drupal\media_parent_entity_link\CacheContext;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class MediaParentEntityCacheContext.
 */
class MediaParentEntityCacheContext implements CacheContextInterface {


  /**
   * Constructs a new MediaParentEntityCacheContext object.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    \Drupal::messenger()->addMessage('Lable of cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($parententity = NULL) {
    return $parententity;
    if($parententity !== NULL) {
      return $parententity->bundle() . '-' . $parententity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
