<?php

/**
 * Add field formatters to use with media parent entity link.
 *
 * You still need to verify if the formatter works with the implementation
 * of media parent entity link. This might not be the case for every formatter.
 *
 * @param $formatters
 *   the formatters to be allowed.
 *
 * @return void
 */
function hook_media_parent_entity_link_alter_formatters(&$formatters) {
  $formatters[] = 'blazy';
}
